const remote = require('electron').remote
const application = remote.app
var Datastore = require('nedb')
<<<<<<< HEAD
var dbFRSettings = new Datastore({ filename: `${application.getPath('userData')}/dbFRSettings.db`})
dbFRSettings.loadDatabase(function (err) { 
});
var dbMain = new Datastore({ filename: `${application.getPath('userData')}/dbMain.db`})
dbMain.loadDatabase(function (err) { 
});

import {getFiscalRegistersFromBase} from '../baseGetters/getFiscalRegisters'
import {getCurrentFiscalRegisterFromBase} from '../baseGetters/getCurrentFiscalRegisterFromBase'
import {setCurrentFiscalRegisterToBase} from '../baseGetters/setCurrentFiscalRegisterToBase'
import {getCurrentFRSettings} from '../baseGetters/getCurrentFRSettings'
=======
var db = new Datastore({ filename: `${application.getPath('userData')}/base.db`})
db.loadDatabase(function (err) { 
});

import {getFiscalRegistersFromBase} from '../baseGetters/getFiscalRegisters'
>>>>>>> cm/master
import {addFiscalRegisterToBase} from '../baseGetters/addFiscalRegister'
import {deleteFiscalRegisterFromBase} from '../baseGetters/deleteFiscalRegister'

const state = {
  fiscalRegisters: [],
<<<<<<< HEAD
  currentFR: 0,
  currentFRSettings: {
    model: 'LIBFPTR_MODEL_ATOL_AUTO',  
    connection: 'LIBFPTR_PORT_COM',
    comFile: "COM1",
    baudRate: 'LIBFPTR_PORT_BR_115200', 
    IPAddress: "192.168.0.15", 
    IPPort: 5555,            
  },
=======
  currentFR: 0
>>>>>>> cm/master
}

const mutations = {
  SET_FISCAL_REGISTERS (state, payload) {
    state.fiscalRegisters = payload
  },
  SET_CURRENT_FR(state, payload) {
<<<<<<< HEAD
    console.log(payload)
    state.currentFR = payload
  },
  SET_CURRENT_FR_SETTINGS(state, payload) {
    console.log(payload)
    state.currentFRSettings = payload
=======
    state.currentFR = payload
>>>>>>> cm/master
  }
}

const actions = {
  getFiscalRegisters ({ commit }) {
<<<<<<< HEAD
    getFiscalRegistersFromBase(dbFRSettings).then(result => {
=======
    getFiscalRegistersFromBase(db).then(result => {
>>>>>>> cm/master
      console.log(result)
      commit('SET_FISCAL_REGISTERS', result)
    });
  },
  addFiscalRegister ({ commit, dispatch }, settings) {
<<<<<<< HEAD
    addFiscalRegisterToBase(dbFRSettings, settings).then(result => {
=======
    addFiscalRegisterToBase(db, settings).then(result => {
>>>>>>> cm/master
      dispatch('getFiscalRegisters')
    });
  },
  deleteFiscalRegister ({ commit, dispatch }, id) {
<<<<<<< HEAD
    deleteFiscalRegisterFromBase(dbFRSettings, id).then(result => {
=======
    deleteFiscalRegisterFromBase(db, id).then(result => {
>>>>>>> cm/master
      dispatch('getFiscalRegisters')
    });
  },
  saveCurrentFR({ commit }, fr) {
<<<<<<< HEAD
    console.log(fr)
    commit('SET_CURRENT_FR', fr)
  },
  getCurrentFR({ commit }) {
    getCurrentFiscalRegisterFromBase(dbMain).then(result => {
      commit('SET_CURRENT_FR', result)
    });
  },
  setCurrentFR({ commit }, id) {
    setCurrentFiscalRegisterToBase(dbMain, id).then(result => {
      commit('SET_CURRENT_FR', result)
    });
  },
  setCurrentFRSettings({ commit, state }) {
    getCurrentFRSettings(dbFRSettings, state.currentFR ).then(result => {
      commit('SET_CURRENT_FR_SETTINGS', result)
    });
=======
    commit('SET_CURRENT_FR', fr)
>>>>>>> cm/master
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
